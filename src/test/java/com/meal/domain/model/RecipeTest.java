package com.meal.domain.model;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class RecipeTest {

    private static final String NAME = "NAME";
    private static final String SUBTITLE = "SUBTITLE";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final Image IMAGE = Image.getBuilder().withName(NAME).build();

    @Test
    @DisplayName("Building a recipe works properly.")
    void build() {
        final var recipe = Recipe.getBuilder().withName(NAME).withSubtitle(SUBTITLE).withDescription(DESCRIPTION).withImage(IMAGE).build();

        SoftAssertions.assertSoftly(softly -> {
            assertThat(recipe.getId()).as("id").isNotNegative();
            assertThat(recipe.getName()).as("name").isEqualTo(NAME);
            assertThat(recipe.getSubtitle()).as("subtitle").isEqualTo(SUBTITLE);
            assertThat(recipe.getDescription()).as("description").isEqualTo(DESCRIPTION);
            assertThat(recipe.getImage()).as("image").isEqualTo(IMAGE);
        });
    }

    @Test
    @DisplayName("Null name throws error")
    void build_null_name() {
        final var builder = Recipe.getBuilder().withName(null).withSubtitle(SUBTITLE).withDescription(DESCRIPTION).withImage(IMAGE);
        assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    @DisplayName("Null subtitle throws error")
    void build_null_subtitle() {
        final var builder = Recipe.getBuilder().withName(NAME).withSubtitle(null).withDescription(DESCRIPTION).withImage(IMAGE);
        assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    @DisplayName("Null description throws error")
    void build_null_description() {
        final var builder = Recipe.getBuilder().withName(NAME).withSubtitle(SUBTITLE).withDescription(null).withImage(IMAGE);
        assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    @DisplayName("Null image throws error")
    void build_null_image() {
        final var builder = Recipe.getBuilder().withName(NAME).withSubtitle(SUBTITLE).withDescription(DESCRIPTION).withImage(null);
        assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
    }
}