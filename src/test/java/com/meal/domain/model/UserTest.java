package com.meal.domain.model;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class UserTest {

    private static final String NAME = "NAME";

    @Test
    @DisplayName("Building a user works properly.")
    void build() {
        final var user = User.getBuilder().withName(NAME).build();

        SoftAssertions.assertSoftly(softly -> {
            assertThat(user.getId()).as("id").isNotNegative();
            assertThat(user.getName()).as("name").isEqualTo(NAME);
        });
    }

    @Test
    @DisplayName("Null name throws error")
    void build_null_name() {
        final var builder = User.getBuilder().withName(null);
        assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
    }

}