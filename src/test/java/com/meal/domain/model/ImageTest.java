package com.meal.domain.model;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ImageTest {

    private static final String NAME = "NAME";

    @Test
    @DisplayName("Building an image works properly.")
    void build() {
        final var entity = Image.getBuilder().withName(NAME).build();

        SoftAssertions.assertSoftly(softly -> assertThat(entity.getName()).as("name").isEqualTo(NAME));
    }

    @Test
    @DisplayName("Null name throws error")
    void build_null_name() {
        final var builder = Image.getBuilder().withName(null);
        assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
    }
}