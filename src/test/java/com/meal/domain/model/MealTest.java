package com.meal.domain.model;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MealTest {

    private static final String NAME = "NAME";
    private static final String SUBTITLE = "SUBTITLE";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final Image IMAGE = Image.getBuilder().withName(NAME).build();

    private static final Recipe RECIPE = Recipe.getBuilder().withName(NAME).withSubtitle(SUBTITLE).withDescription(DESCRIPTION).withImage(IMAGE).build();

    @Test
    @DisplayName("Building a meal works properly.")
    void build() {
        final var meal = Meal.getBuilder().withRecipe(RECIPE).build();

        SoftAssertions.assertSoftly(softly -> {
            assertThat(meal.getId()).as("id").isNotNegative();
            assertThat(meal.getRecipe()).as("recipe").isEqualTo(RECIPE);
        });
    }

    @Test
    @DisplayName("Null recipe throws error")
    void build_null_recipe() {
        final var builder = Meal.getBuilder().withRecipe(null);
        assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
    }
}