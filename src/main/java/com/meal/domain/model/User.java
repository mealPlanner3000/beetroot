package com.meal.domain.model;

import java.util.concurrent.atomic.AtomicLong;

/**
 * This class models a user.
 */
public final class User {

    /**
     * The unique identifier of a user
     */
    private final long id;

    /**
     * The users name. Does not need to be unique
     */
    private final String name;

    private User(long id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Returns the builder.
     *
     * @return the builder
     */
    public static Builder getBuilder() {
        return new Builder();
    }

    /**
     * Returns the unique id of the user.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Returns the name of the user.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * The builder for a user
     */
    public static final class Builder {

        private static final AtomicLong ID_GENERATOR = new AtomicLong();
        private String name;

        private Builder() {
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public User build() {
            assertNotNull(name, "name");
            return new User(ID_GENERATOR.getAndIncrement(), name);
        }

        private void assertNotNull(final Object object, final String name) {
            if (object == null) {
                throw new IllegalArgumentException(String.format("Called build with a null '%s' parameter.", name));
            }
        }
    }
}
