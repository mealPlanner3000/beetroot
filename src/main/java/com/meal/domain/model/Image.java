package com.meal.domain.model;

/**
 * Models an image.
 */
public final class Image {

    /**
     * The name of the image.
     */
    private final String name;

    private Image(String name) {
        this.name = name;
    }

    /**
     * Returns a builder for an image.
     *
     * @return the builder
     */
    public static Builder getBuilder() {
        return new Builder();
    }

    /**
     * Returns the name of the image.
     *
     * @return the name of the image
     */
    public String getName() {
        return name;
    }

    /**
     * The builder for an image.
     */
    public static final class Builder {
        private String name;

        private Builder() {
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Image build() {
            assertNotNull(name, "name");
            return new Image(name);
        }

        private void assertNotNull(final Object object, final String name) {
            if (object == null) {
                throw new IllegalArgumentException(String.format("Called build with a null '%s' parameter.", name));
            }
        }
    }
}
