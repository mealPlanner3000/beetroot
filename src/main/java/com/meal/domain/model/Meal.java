package com.meal.domain.model;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Models a meal.
 * <p>
 * A meal is the allocation of a {@link Recipe} to a {@link User} on a specific {@link Slot}.
 * The meal contains customizations of the {@link Recipe} to fit the {@link User}s needs.
 */
public final class Meal {

    /**
     * The unique id of a meal.
     */
    private final long id;

    /**
     * The recipe assigned to this meal
     */
    private final Recipe recipe;

    private Meal(long id, Recipe recipe) {
        this.id = id;
        this.recipe = recipe;
    }

    /**
     * Returns the builder.
     *
     * @return the builder for a meal
     */
    public static Builder getBuilder() {
        return new Builder();
    }

    /**
     * Returns the unique id of the meal.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Returns the recipe assigned to the meal.
     *
     * @return the recipe
     */
    public Recipe getRecipe() {
        return recipe;
    }

    /**
     * The builder of a meal
     */
    public static final class Builder {

        private static final AtomicLong ID_GENERATOR = new AtomicLong();
        private Recipe recipe;

        private Builder() {
        }

        /**
         * Sets the recipe of the meal
         *
         * @param recipe the recipe
         * @return the builder
         */
        public Builder withRecipe(Recipe recipe) {
            this.recipe = recipe;
            return this;
        }

        /**
         * Creates a new meal.
         *
         * @return the new meal
         */
        public Meal build() {
            assertNotNull(this.recipe, "recipe");

            return new Meal(ID_GENERATOR.getAndIncrement(), recipe);
        }

        private void assertNotNull(final Object object, final String name){
            if(object == null){
                throw new IllegalArgumentException(String.format("Called build with a null '%s' parameter.", name));
            }
        }
    }
}
