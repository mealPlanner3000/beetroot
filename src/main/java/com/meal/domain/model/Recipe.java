package com.meal.domain.model;

import java.util.concurrent.atomic.AtomicLong;

/**
 * The recipe models an abstract guide to transform a set of {@link Ingredients} into a
 * delicious pile of carbohydrates, lipids and protein.
 */
public final class Recipe {

    /**
     * The unique id of the recipe
     */
    private final long id;

    /**
     * The name of the recipe
     */
    private final String name;

    /**
     * A subtitle of the recipes name
     */
    private final String subtitle;

    /**
     * The description of the recipe
     */
    private final String description;

    /**
     * An image visualizing the final result
     */
    private final Image image;

    private Recipe(long id, String name, String subtitle, String description, Image image) {
        this.id = id;
        this.name = name;
        this.subtitle = subtitle;
        this.description = description;
        this.image = image;
    }

    /**
     * Returns the builder
     *
     * @return the builder
     */
    public static Builder getBuilder() {
        return new Builder();
    }

    /**
     * Returns the unique id of the recipe.
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Returns the name of the recipe
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the subtitle of the recipe
     *
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * Returns the description of the recipe
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the image of the final result
     *
     * @return the image
     */
    public Image getImage() {
        return image;
    }

    /**
     * The builder of a new recipe
     */
    public static final class Builder {

        private static final AtomicLong ID_GENERATOR = new AtomicLong();
        private String name;
        private String subtitle;
        private String description;
        private Image image;

        private Builder() {
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withSubtitle(String subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withImage(Image image) {
            this.image = image;
            return this;
        }

        public Recipe build() {
            assertNotNull(name, "name");
            assertNotNull(subtitle, "subtitle");
            assertNotNull(description, "Description");
            assertNotNull(image, "image");
            return new Recipe(ID_GENERATOR.getAndIncrement(), name, subtitle, description, image);
        }

        private void assertNotNull(final Object object, final String name){
            if(object == null){
                throw new IllegalArgumentException(String.format("Called build with a null '%s' parameter.", name));
            }
        }
    }
}
